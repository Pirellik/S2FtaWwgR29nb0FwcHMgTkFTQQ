FROM golang:1.17.1 AS build

ENV CGO_ENABLED=0
ENV GO111MODULE=on
ENV GOOS=linux

WORKDIR /app
COPY . .

RUN go mod download
RUN go build -o ./bin/url-collector cmd/url-collector/*.go

FROM alpine:latest

COPY --from=build /app/bin/url-collector /app/url-collector

CMD ["/app/url-collector"]