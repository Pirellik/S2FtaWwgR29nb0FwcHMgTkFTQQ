package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/rs/zerolog/log"
)

const (
	picture string = "{\"hdurl\":\"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg\"}"
)

func getPicture(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	log.Info().Str("path", r.RequestURI).Msg("Received new request")
	fmt.Fprint(w, picture)
}

func main() {
	port := ":8081"
	envPort := os.Getenv("NASA_API_MOCK_PORT")
	if envPort != "" {
		port = ":" + envPort
	}
	http.HandleFunc("/planetary/apod", getPicture)
	log.Info().Msg("Starting mocked NASA API")
	err := http.ListenAndServe(port, nil)
	log.Fatal().Err(err).Send()
}
