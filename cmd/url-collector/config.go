package main

import (
	"url-collector/pkg/nasa"

	"github.com/Netflix/go-env"
	"github.com/rs/zerolog/log"
)

type Config struct {
	Port          string `env:"PORT,default=8080"`
	NasaClientCfg nasa.Config
}

func LoadConfig() Config {
	var config Config
	_, err := env.UnmarshalFromEnviron(&config)
	if err != nil {
		log.Error().Err(err).Msg("Reading configuration from env returned an error")
	}
	return config
}
