package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"url-collector/pkg/iso"

	"github.com/rs/zerolog/log"
)

type collectorContext struct {
	pictureSvc PictureService
}

type PictureService interface {
	GetPictures(from, to iso.Date) (pictureURLs []string, err error)
}

type queryParams struct {
	from iso.Date
	to   iso.Date
}

func (q *queryParams) valid() bool {
	return !q.to.Before(q.from.Time)
}

type response struct {
	URLs []string `json:"urls"`
}

func NewCollectorCtx(ps PictureService) collectorContext {
	return collectorContext{
		pictureSvc: ps,
	}
}

func (c *collectorContext) GetPictures(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	log.Info().Str("path", r.RequestURI).Msg("Received new request")

	p, err := parseQueryParams(r.URL.Query())
	if err != nil || !p.valid() {
		log.Info().Interface("queryParams", p).
			Msg("Invalid query params")
		badRequest(w, "invalid query params")
		return
	}

	pics, err := c.pictureSvc.GetPictures(p.from, p.to)
	if err != nil {
		log.Error().Err(err).Interface("queryParams", p).
			Msg("Could not get pictures for specified query params")
		internalError(w)
		return
	}

	res := response{URLs: pics}
	log.Info().Interface("response", res).Msg("Sending response")
	ok(w, res)
}

func parseQueryParams(v url.Values) (queryParams, error) {
	if len(v["from"]) == 0 || len(v["to"]) == 0 {
		return queryParams{}, errors.New("required query params (from, to) not present")
	}
	from, err := iso.ParseDate(v["from"][0])
	if err != nil {
		return queryParams{}, err
	}
	to, err := iso.ParseDate(v["to"][0])
	if err != nil {
		return queryParams{}, err
	}
	return queryParams{
		from: from,
		to:   to,
	}, nil
}

func badRequest(w http.ResponseWriter, msg string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	fmt.Fprintf(w, "{\"error\":\"%v\"}", msg)
}

func ok(w http.ResponseWriter, resp interface{}) {
	w.Header().Set("Content-Type", "application/json")
	b, err := json.Marshal(resp)
	if err != nil {
		internalError(w)
	}
	_, err = w.Write(b)
	if err != nil {
		log.Error().Err(err).Msg("Failed to send response")
	}
}

func internalError(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	_, err := w.Write([]byte("{\"error\":\"internal server error\"}"))
	if err != nil {
		log.Error().Err(err).Msg("Failed to send error response")
	}
}
