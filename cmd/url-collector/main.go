package main

import (
	"net/http"
	service "url-collector/internal/picture-service"
	"url-collector/pkg/nasa"

	"github.com/rs/zerolog/log"
)

func main() {
	cfg := LoadConfig()
	clientNASA := nasa.NewClient(cfg.NasaClientCfg)
	picSvc := service.NewPictureService(clientNASA)
	c := NewCollectorCtx(picSvc)
	http.HandleFunc("/pictures", c.GetPictures)

	log.Info().Interface("config", cfg).Msg("Starting url-collector service")

	err := http.ListenAndServe(":"+cfg.Port, nil)
	log.Fatal().Err(err).Send()
}
