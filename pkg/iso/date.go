package iso

import "time"

const (
	dateLayout string = "2006-01-02"
)

type Date struct {
	time.Time
}

func NewDate(year, month, day int, loc *time.Location) Date {
	if loc == nil {
		loc = time.UTC
	}
	return Date{
		time.Date(year, time.Month(month), day, 0, 0, 0, 0, loc),
	}
}

func ParseDate(in string) (Date, error) {
	parsed, err := time.Parse(dateLayout, in)
	if err != nil {
		return Date{}, err
	}
	return Date{Time: parsed}, nil
}

func (d *Date) String() string {
	return d.Format(dateLayout)
}
