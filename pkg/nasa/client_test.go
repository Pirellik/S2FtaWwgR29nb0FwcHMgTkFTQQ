package nasa

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
	"url-collector/pkg/iso"

	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
)

func getPicture(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	log.Info().Str("path", r.RequestURI).Msg("Received new request")
	fmt.Fprint(w, "{\"hdurl\":\"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg\"}")
}

func getPictureUnavailable(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	log.Info().Str("path", r.RequestURI).Msg("Received new request")
	fmt.Fprint(w, "{\"unavailable\"}")
}

func TestGetPictures(t *testing.T) {
	cfg := Config{
		ConcurrentRequests: 5,
		ApiKey:             "test",
	}

	t.Run("should return error when cannot connect to the NASA API", func(t *testing.T) {
		srv := httptest.NewUnstartedServer(http.HandlerFunc(getPicture))
		defer srv.Close()
		cfg.NasaApiUrl = srv.URL

		client := NewClient(cfg)
		from := iso.NewDate(2021, 10, 24, time.UTC)
		to := iso.NewDate(2021, 10, 24, time.UTC)

		_, err := client.GetPictures(from, to)

		assert.Error(t, err)
	})

	t.Run("should return error when to date before from date", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(getPicture))
		defer srv.Close()
		cfg.NasaApiUrl = srv.URL

		client := NewClient(cfg)
		from := iso.NewDate(2021, 10, 25, time.UTC)
		to := iso.NewDate(2021, 10, 24, time.UTC)

		_, err := client.GetPictures(from, to)

		assert.Error(t, err)
	})

	t.Run("should return error in case of unexpected response", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(getPictureUnavailable))
		defer srv.Close()
		cfg.NasaApiUrl = srv.URL

		client := NewClient(cfg)
		from := iso.NewDate(2021, 10, 24, time.UTC)
		to := iso.NewDate(2021, 10, 30, time.UTC)

		_, err := client.GetPictures(from, to)

		assert.Error(t, err)
	})

	t.Run("should return picture URL for each day", func(t *testing.T) {
		srv := httptest.NewServer(http.HandlerFunc(getPicture))
		defer srv.Close()
		cfg.NasaApiUrl = srv.URL

		wantPicURLs := []string{
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
			"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg",
		}
		client := NewClient(cfg)
		from := iso.NewDate(2021, 10, 24, time.UTC)
		to := iso.NewDate(2021, 10, 30, time.UTC)

		picURLs, err := client.GetPictures(from, to)

		assert.NoError(t, err)
		assert.Equal(t, wantPicURLs, picURLs)
	})
}
