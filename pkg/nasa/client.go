package nasa

import (
	"encoding/json"
	"errors"
	"net/http"
	"sync"
	"time"
	"url-collector/pkg/iso"

	"github.com/rs/zerolog/log"
)

const (
	pictureEndpoint string = "/planetary/apod"
	hoursInDay      int    = 24
)

type Client struct {
	cfg       Config
	semaphore chan struct{}
}

type Config struct {
	NasaApiUrl         string `env:"NASA_API_URL,default=https://api.nasa.gov/planetary/apod"`
	ApiKey             string `env:"API_KEY,default=DEMO_KEY"`
	ConcurrentRequests int    `env:"CONCURRENT_REQUESTS,default=5"`
}

type response struct {
	URL string `json:"hdurl"`
}

func NewClient(c Config) *Client {
	return &Client{
		cfg:       c,
		semaphore: make(chan struct{}, c.ConcurrentRequests),
	}
}

func (c *Client) GetPictures(from, to iso.Date) (pictureURLs []string, err error) {
	if to.Before(from.Time) {
		return nil, errors.New("to date before from date")
	}
	numDays := int(to.Sub(from.Time).Hours())/(hoursInDay) + 1
	pictureURLs = make([]string, 0, numDays)
	picChannel := make(chan string, numDays)
	errChannel := make(chan error, numDays)
	var wg sync.WaitGroup

	for d := from; !to.Before(d.Time); d.Time = d.Add(time.Hour * time.Duration(hoursInDay)) {
		wg.Add(1)
		c.semaphore <- struct{}{}
		go c.getOnePicture(d, picChannel, errChannel, &wg)
	}
	wg.Wait()
	close(picChannel)
	close(errChannel)

	for picURL := range picChannel {
		pictureURLs = append(pictureURLs, picURL)
	}
	for err := range errChannel {
		if err != nil {
			return pictureURLs, errors.New("atleast one NASA API call has failed")
		}
	}

	return pictureURLs, nil
}

func (c *Client) getOnePicture(
	date iso.Date,
	picChannel chan<- string,
	errChannel chan<- error,
	wg *sync.WaitGroup,
) {
	queryURL := c.cfg.NasaApiUrl + pictureEndpoint + "?api_key=" + c.cfg.ApiKey + "&date=" + date.String()
	log.Info().Str("apiURL", queryURL).Msg("Sending request to NASA API")

	resp, err := makeGETPictureCall(queryURL)
	log.Err(err).Interface("response", resp).Msg("Response from NASA API")

	picChannel <- resp.URL
	errChannel <- err
	<-c.semaphore
	wg.Done()
}

func makeGETPictureCall(URL string) (response, error) {
	resp, err := http.Get(URL)
	if err != nil {
		return response{}, err
	}
	var decoded response
	err = json.NewDecoder(resp.Body).Decode(&decoded)
	if err != nil || decoded.URL == "" {
		return response{}, errors.New("failed to decode picture URL from the response")
	}
	return decoded, nil
}
