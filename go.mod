module url-collector

go 1.16

require (
	github.com/Netflix/go-env v0.0.0-20210215222557-e437a7e7f9fb
	github.com/rs/zerolog v1.25.0
	github.com/stretchr/testify v1.7.0
)
