GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

.PHONY: test
test: ## Run tests.
	@go test ./...

.PHONY: lint
lint: ## Run linter.
	@golangci-lint run ./...

.PHONY: run
run: ## Run service with real dependencies.
	@docker-compose --env-file real.env up --build

.PHONY: run-mocked
run-mocked: ## Run service with mocked dependencies.
	@docker-compose --env-file mocked.env up --build

.PHONY: help
help: ## Show this help.
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  ${YELLOW}%-34s${GREEN}%s${RESET}\n", $$1, $$2}' $(MAKEFILE_LIST)