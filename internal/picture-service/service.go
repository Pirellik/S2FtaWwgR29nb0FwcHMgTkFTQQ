package service

import "url-collector/pkg/iso"

type PictureProvider interface {
	GetPictures(from, to iso.Date) (pictureURLs []string, err error)
}

type PictureService struct {
	provider PictureProvider
}

func NewPictureService(pp PictureProvider) *PictureService {
	return &PictureService{
		provider: pp,
	}
}

func (ps *PictureService) GetPictures(from, to iso.Date) (pictureURLs []string, err error) {
	return ps.provider.GetPictures(from, to)
}
